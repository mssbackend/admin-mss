/* App.js */
import React, { Component } from "react";
import CanvasJSReact from "./canvasjs.react";
// import moment  from "moment"
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default class LineChart extends Component {
 
  render() {
    var dataPoints = [];
    for (let i = 0; i < this.props.data.length; i++) {
      const element = this.props.data[i];      
      dataPoints.push({ x:new Date(element["date"]), y: element["count"] });
    }
    const options = {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light2", // "light1", "dark1", "dark2"
      title: {
        text: this.props.title,
        //fontColor:"#6088ea",
        fontSize: 25,
        fontWeight: "bolder"
      },
      axisY: {
        title: this.props.title,
        includeZero: true,
        suffix: ""
      },
      axisX: {
        title: "Last 10 days",
        valueFormatString: "MMM D",
        prefix: "",
        interval: 2
      },
      data: [
        {
          type: this.props.chartType,
          toolTipContent: "{x}: {y}",
          dataPoints: dataPoints
        }
      ]
    };
    return (
      <div>
        <CanvasJSChart
          options={options}
          /* onRef={ref => this.chart = ref} */
        />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
}
//module.exports = LineChart;

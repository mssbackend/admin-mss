import React, { Component } from "react";
import CanvasJSReact from "./canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
export default class PieChart extends Component {
  render() {
    var dataPoints = [];
    for (let i = 0; i < this.props.data.length; i++) {
      const element = this.props.data[i];
      dataPoints.push({ y: element["value"], label: element["name"] });
    }
    
    const options = {
      exportEnabled: true,
      animationEnabled: true,
      theme: "light2",
      title: {
        text: this.props.title
      },
      data: [
        {
          type: "pie",
          startAngle: 75,
          toolTipContent: "<b>{label}</b> ({y})",
          showInLegend: "true",
          legendText: "{label}",
          indexLabelFontSize: 16,
          indexLabel: "{label} ({y})",
          dataPoints: dataPoints
        }
      ]
    };
    return (
      <div>
        <CanvasJSChart
          options={options}
          /* onRef={ref => this.chart = ref} */
        />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
}

import React, { Component } from "react";
import Viewer from "react-viewer/dist/index.js";
export default class ImageViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }

  render() {
    const images = [
      {
        src: this.props.src
      }
    ];
    return (
      <div>
        <img
          onClick={() => {
            this.setState({ visible: true });
          }}
          src={this.props.src}
          alt="quixote"
          className="z-depth-1 rounded mb-md-0 mb-2"
        />

        <Viewer
          visible={this.state.visible}
          onClose={() => {
            this.setState({ visible: false });
          }}
          images={images}
        />
      </div>
    );
  }
}

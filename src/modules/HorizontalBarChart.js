import React, { Component } from "react";
import CanvasJSReact from "./canvasjs.react";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class HorizontalBarChart extends Component {
  render() {
    var dataPoints = [];
    for (let i = 0; i < this.props.data.length; i++) {
      const element = this.props.data[i];
      dataPoints.push({ y: element["value"], label: element["name"] });
    }

    const options = {
      animationEnabled: true,
      theme: "light2",
      title: {
        text: this.props.title
      },
      axisX: {
        title: this.props.Xaxistitle,
        reversed: true
      },
      axisY: {
        title: this.props.Yaxistitle,
        labelFormatter: this.addSymbols
      },
      data: [
        {
          type: "bar",
          dataPoints: dataPoints
        }
      ]
    };
    return (
      <div>
        <CanvasJSChart
          options={options}
          /* onRef={ref => this.chart = ref} */
        />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
  addSymbols(e) {
    var suffixes = ["", "K", "M", "B"];
    var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
    if (order > suffixes.length - 1) order = suffixes.length - 1;
    var suffix = suffixes[order];
    return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
  }
}

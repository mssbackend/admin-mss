export default class Globals {
  checkTokenNotExpired() {
    var accessToken =
      localStorage.getItem("accessToken") != null
        ? JSON.parse(localStorage.getItem("accessToken"))
        : null;
    if (accessToken != null) {
      var expiry = Date.parse(accessToken["expiry"]);
      if (expiry > Date.now()) {
        return true;
      }
    }
    return false;
  }
}

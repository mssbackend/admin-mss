import React, { Component } from "react";
import LineChart from "../modules/LineChart.js";
import NetworkUtil from "../networkUtil/NetworkUtil";
import { browserHistory } from "react-router";

export default class HomeComponents extends Component {
  mounted = true;
  state = {
    isDataloaded: false,
    schools: "",
    students: "",
    tutors: "",
    activeUsers: {
      last10days: []
    },
    sharedPosts: {
      last10days: []
    },
    sharedfiles: {
      last10days: []
    },
    likes: {
      last10days: []
    },
    comments: {
      last10days: []
    }
  };
  constructor(props) {
    super(props);
    this.getDataforhome();
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  navigateToSchools = () => {
    browserHistory.push("/main/schools");
  };
  navigateToOnlineUsers = () => {
    browserHistory.push("/main/activeusers");
  };
  navigateToUsersAcitivity() {
    browserHistory.push("/main/usersactivity");
  }
  navigateToSharedFiles() {
    browserHistory.push("/main/sharedfiles");
  }
  navigateToLikes() {
    browserHistory.push("/main/likes");
  }
  navigateToComments() {
    browserHistory.push("/main/comments");
  }
  async getDataforhome() {
    var response = await new NetworkUtil().httpGet("admin/GetDataForHome");
    if (response != null) {
      if (response.status === 200) {
        const responseJson = await response.json();
        if (responseJson["success"]) {
          if (this.mounted) {
            this.setState(responseJson["data"]);
            this.setState({ isDataloaded: true });
          }
        }
      }
    }
  }
  render() {
    return (
      <div>
        <div className="container mb-5 mt-4">
          <div className="row justify-content-md-center">
            <div className="col-md-auto">
              <div
                className="card"
                onClick={this.navigateToSchools}
                style={{ width: "12rem", cursor: "pointer" }}
              >
                <div className="card-body">
                  <h2 className="card-title text-center mb-0">
                    {this.state.schools}
                  </h2>
                  <p className="card-text text-center">Schools</p>
                </div>
              </div>
            </div>
            <div className="col-md-auto">
              <div className="card " style={{ width: "12rem" }}>
                <div className="card-body">
                  <h2 className="card-title text-center mb-0">
                    {this.state.students}
                  </h2>
                  <p className="card-text text-center">Students</p>
                </div>
              </div>
            </div>
            <div className="col-md-auto">
              <div className="card " style={{ width: "12rem" }}>
                <div className="card-body">
                  <h2 className="card-title text-center mb-0">
                    {this.state.tutors}
                  </h2>
                  <p className="card-text text-center">Teachers</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.state.isDataloaded ? (
          <div>
            <div className="container mb-5">
              <div className="row">
                <div className="col-md-6">
                  <div className="card shadow">
                    <div
                      onClick={this.navigateToOnlineUsers}
                      className="pt-2 pl-2"
                      style={{ cursor: "pointer" }}
                    >
                      <h5>
                        Total active users :
                        <span className="text-primary">
                          {this.state.activeUsers["total"]}
                        </span>
                      </h5>
                      <h5>
                        Daily active users :
                        <span className="text-primary">
                          {this.state.activeUsers["today"]}
                        </span>
                      </h5>
                    </div>
                    <div className="card-body  p-0">
                      <LineChart
                        title={"Online users"}
                        chartType={"line"}
                        data={this.state.activeUsers["last10days"]}
                      ></LineChart>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card shadow">
                    <div
                      className="pt-2 pl-2"
                      onClick={this.navigateToUsersAcitivity}
                      style={{ cursor: "pointer" }}
                    >
                      <h5>
                        Total shared posts :
                        <span className="text-primary">
                          {this.state.sharedPosts["total"]}
                        </span>
                      </h5>
                      <h5>
                        Daily shared posts:
                        <span className="text-primary">
                          {this.state.sharedPosts["today"]}
                        </span>
                      </h5>
                    </div>
                    <div className="card-body  p-0">
                      <LineChart
                        className="anchor"
                        title={"Users activity"}
                        chartType={"spline"}
                        data={this.state.sharedPosts["last10days"]}
                      ></LineChart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mb-5">
              <div className="row justify-content-md-center">
                <div className="col">
                  <div className="card shadow">
                    <div
                      className="pt-2 pl-2"
                      onClick={this.navigateToSharedFiles}
                      style={{ cursor: "pointer" }}
                    >
                      <h5>
                        Total shared files :
                        <span className="text-primary">
                          {this.state.sharedfiles["total"]}
                        </span>
                      </h5>
                      <h5>
                        Daily shared files :
                        <span className="text-primary">
                          {this.state.sharedfiles["today"]}
                        </span>
                      </h5>
                    </div>
                    <div className="card-body  p-0">
                      <LineChart
                        title={"File sharing"}
                        chartType={"spline"}
                        data={this.state.sharedfiles["last10days"]}
                      ></LineChart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mb-5">
              <div className="row">
                <div className="col-md-6">
                  <div className="card shadow">
                    <div
                      className="pt-2 pl-2"
                      onClick={this.navigateToLikes}
                      style={{ cursor: "pointer" }}
                    >
                      <h5>
                        Total likes :
                        <span className="text-primary">
                          {this.state.likes["total"]}
                        </span>
                      </h5>
                      <h5>
                        Today likes :
                        <span className="text-primary">
                          {this.state.likes["today"]}
                        </span>
                      </h5>
                    </div>
                    <div className="card-body  p-0">
                      <LineChart
                        title={"Likes trending"}
                        chartType={"spline"}
                        data={this.state.likes["last10days"]}
                      ></LineChart>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card shadow">
                    <div
                      className="pt-2 pl-2"
                      onClick={this.navigateToComments}
                      style={{ cursor: "pointer" }}
                    >
                      <h5>
                        Total shared comments :
                        <span className="text-primary">
                          {this.state.comments["total"]}
                        </span>
                      </h5>
                      <h5>
                        Daily shared comments:
                        <span className="text-primary">
                          {this.state.comments["today"]}
                        </span>
                      </h5>
                    </div>
                    <div className="card-body  p-0">
                      <LineChart
                        className="anchor"
                        title={"Shared comments"}
                        chartType={"spline"}
                        data={this.state.comments["last10days"]}
                      ></LineChart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="row justify-content-md-center">
              <div className="col-md-auto">
                <div
                  className="spinner-border"
                  style={{ width: "3rem", height: "3rem" }}
                  role="status"
                >
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

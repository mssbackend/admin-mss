import React, { Component } from "react";
import "../styles/Home.css";
import HomeComponents from "../component/HomeComponents";
import Globals from "../modules/globals";
import {browserHistory } from "react-router";

export default class Home extends Component {
  constructor(props) {
    super(props);
    if (!new Globals().checkTokenNotExpired()) {
      browserHistory.push("/")
    }
  }
  render() {
    return (
      <div>
        <HomeComponents />
        <div>{this.props.children}</div>
      </div>
    );
  }
}

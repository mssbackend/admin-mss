import React, { Component } from "react";
import Menu from "../component/Menu.js";

export default class Main extends Component {
  render() {
    return (
      <div>
        <Menu title={"My Smart School"}></Menu>
        <div>{this.props.children}</div>
      </div>
    );
  }
}

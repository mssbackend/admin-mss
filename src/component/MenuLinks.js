import React, { Component } from "react";
import "../styles/MenuLinks.css";

export default class MenuLinks extends Component {
  constructor(props) {
    super(props);
    // Any number of links can be added here
    this.state = {
      links: [
        {
          text: "Home",
          link: "/main/home",
          icon: "fa-home"
        },
        {
          text: "Schools",
          link: "/main/home",
          icon: "fa-institution"
        },
        {
          text: "Students",
          link: "/main/home",
          icon: "fa-group"
        }
      ]
    };
  }
  render() {
    let links = this.state.links.map((link, i) => (
      <li ref={i + 1} key ={i} >
        <i aria-hidden="true" className={`fa ${link.icon}`}></i>
        <a href={link.link}>
          {link.text}
        </a>
      </li>
    ));

    return (
      <div className={this.props.menuStatus} id="menu">
        <ul>{links}</ul>
      </div>
    );
  }
}

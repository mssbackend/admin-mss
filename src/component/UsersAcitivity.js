import React, { Component } from "react";
import PieChart from "../modules/PieChart";
import HorizontalBarChart from "../modules/HorizontalBarChart";
import { browserHistory } from "react-router";
import "../styles/App.css";
import NetworkUtil from "../networkUtil/NetworkUtil";
import moment from "moment";
import ImageViewer from "../modules/ImageViewer";
export default class UsersActivity extends Component {
  mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      isDataloaded: false,
      bySchools: [],
      byPostType: [],
      recentPosts: []
    };
    this.getData();
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  goBack = () => {
    browserHistory.goBack();
  };
  onChange = e => {
    this.setState({ isDataloaded: false });
    this.getData(e.target.value);
  };

  async getData(action = "today") {
    var response = await new NetworkUtil().httpGet(
      "admin/GetDataForUsersActivityPage?action=" + action
    );
    if (response != null) {
      if (response.status === 200) {
        const responseJson = await response.json();
        if (responseJson["success"]) {
          if (this.mounted) {
            this.setState(responseJson["data"]);
            this.setState({ isDataloaded: true });
          }
        }
      }
    }
  }

  render() {
    return (
      <div>
        <div className="container mt-5">
          <button
            onClick={this.goBack}
            type="button"
            className="btn btn-primary"
          >
            &lt; Go back
          </button>
          <div className="form-group pt-2">
            <div className="col-sm-4 col-md-2 pl-0">
              <select onChange={this.onChange} className="form-control">
                <option value={"today"}>Today</option>
                <option value={"yesterday"}>Yesterday</option>
                <option value={"all"}>All</option>
              </select>
            </div>
          </div>
        </div>
        {this.state.isDataloaded ? (
          <div>
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <div className="card shadow">
                    <div className="card-body  p-0">
                      <HorizontalBarChart
                        title="Users activity by schools"
                        Xaxistitle="Schools"
                        Yaxistitle="Total activities"
                        data={this.state.bySchools}
                      ></HorizontalBarChart>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card shadow">
                    <div className="card-body  p-0">
                      <PieChart
                        title="Type of activities"
                        data={this.state.byPostType}
                      ></PieChart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mt-5">
              <section className="my-5">
                <div className="col-lg-8">
                  <div className="mdb-feed">
                    {this.state.recentPosts.map((value, index) => {
                      return (
                        <div key={index} className="news">
                          <div className="label">
                            {value["profile_pic"] != null ? (
                              <img
                                height="40"
                                width="40"
                                src={value["profile_pic"]}
                                alt="quixote"
                                className="rounded-circle z-depth-1-half"
                              />
                            ) : (
                              <div
                                style={{
                                  height: "40px",
                                  width: "40px",
                                  backgroundColor: "#f5f5f5"
                                }}
                                className="rounded-circle"
                              >
                                <p
                                  className="pt-2"
                                  style={{ textAlign: "center" }}
                                >
                                  {value["name"].toString().substring(0, 1)}
                                </p>
                              </div>
                            )}
                          </div>
                          <div className="excerpt">
                            <div className="brief">
                              <p className="mb-0 text-primary name">
                                {value["name"]}
                              </p>
                              <p className="d-inline pl-2 font-weight-bold">
                                {value["title"]}
                              </p>
                              <div className="d-inline date">
                                {moment(value["date"])
                                  .startOf("hour")
                                  .fromNow()}
                              </div>
                              <p className="d-block mb-2 text-secondary">
                                {" "}
                                {value["school"]}
                              </p>
                              <div className="added-text">
                                {value["content"]}
                              </div>
                              <div className="added-images">
                                {value["content_files"] != null
                                  ? value["content_files"]
                                      .toString()
                                      .split(",")
                                      .map((value, index) => {
                                        return (
                                          <ImageViewer
                                            key={index}
                                            src={value}
                                          />
                                        );
                                      })
                                  : ""}
                              </div>
                            </div>
                            <div className="feed-footer">
                              <p className="d-inline  comment">Comments</p>
                              <p className="pl-1 d-inline">
                                {value["comments"]}
                              </p>
                              &nbsp;&nbsp;
                              <p className="d-inline thumbs">
                                <i className="fa fa-thumbs-up text-primary"></i>
                              </p>
                              <span>
                                <p className="d-inline">{value["likes"]}</p>
                              </span>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </section>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="row justify-content-md-center">
              <div className="col-md-auto">
                <div
                  className="spinner-border"
                  style={{ width: "3rem", height: "3rem" }}
                  role="status"
                >
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

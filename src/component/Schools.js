import React, { Component } from "react";
import { browserHistory } from "react-router";
import NetworkUtil from "../networkUtil/NetworkUtil";
import moment from "moment";
export default class Schools extends Component {
  mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      isDataloaded: false,
      schools: []
    };
    this.getData();
  }
  goBack = () => {
    browserHistory.goBack();
  };
  componentWillUnmount() {
    this.mounted = false;
  }
  async getData() {
    var response = await new NetworkUtil().httpGet(
      "admin/GetDataForSchoolsPage"
    );
    if (response != null) {
      if (response.status === 200) {
        const responseJson = await response.json();
        if (responseJson["success"]) {
          if (this.mounted) {
            this.setState({ schools: responseJson["data"] });
            this.setState({ isDataloaded: true });
          }
        }
      }
    }
  }
  render() {
    return (
      <div>
        <div className="container mt-5 mb-5">
          <button
            onClick={this.goBack}
            type="button"
            className="btn btn-primary"
          >
            &lt; Go back
          </button>
          {this.state.isDataloaded ? (
            this.state.schools.map((value, index) => {
              return (
                <div key={index} className="row mt-3">
                  <div className="col-sm-8 col-md-6">
                    <div className="card shadow">
                      <div className="card-body">
                        <h4 className="card-title">{value["name"]}</h4>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Created at :
                          </p>
                          <h6 className="d-inline">
                            &nbsp;{moment(value["created_at"]).format("LL")}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Created by :
                          </p>
                          <h5 className="d-inline">
                            &nbsp;{value["created_by"]}
                          </h5>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Person position :
                          </p>
                          <h5 className="d-inline">
                            &nbsp;{value["person_position"]}
                          </h5>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">Email :</p>
                          <h5 className="text-primary d-inline">
                            &nbsp;{value["email"]}
                          </h5>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Contact number :
                          </p>
                          <h5 className="d-inline text-primary">
                            &nbsp;{value["mobile_number"]}
                          </h5>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Refered by :
                          </p>
                          <h5 className="d-inline text-primary">
                            &nbsp;{value["referred_by"]}
                          </h5>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Academic year start from :
                          </p>
                          <h6 className="d-inline">
                            &nbsp;
                            {value["acd_year_from"] != null
                              ? moment(value["acd_year_from"]).format("LL")
                              : ""}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Academic year end :
                          </p>
                          <h6 className="d-inline">
                            &nbsp;
                            {value["acd_year_to"] != null
                              ? moment(value["acd_year_to"]).format("LL")
                              : ""}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">Country :</p>
                          <h6 className="d-inline">&nbsp;{value["country"]}</h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">State:</p>
                          <h6 className="d-inline">&nbsp;{value["state"]}</h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">City :</p>
                          <h6 className="d-inline">&nbsp;{value["city"]}</h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Street address :
                          </p>
                          <h6 className="d-inline">
                            &nbsp;{value["street_address"]}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            School website :
                          </p>
                          <h6 className="d-inline">
                            &nbsp;{value["school_website"]}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Time table format :
                          </p>
                          <h6 className="d-inline">
                            &nbsp;{value["time_table_format"]}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">Is active :</p>
                          <h6
                            className={
                              value["isactive"]
                                ? "d-inline text-success"
                                : "d-inline text-danger"
                            }
                          >
                            &nbsp;{value["isactive"] ? "Yes" : "No"}
                          </h6>
                        </div>
                        <div>
                          <p className="card-text mb-1 d-inline">
                            Email verified :
                          </p>
                          <h6
                            className={
                              value["email_verified"]
                                ? "d-inline text-success"
                                : "d-inline text-danger"
                            }
                          >
                            &nbsp;{value["email_verified"] ? "Yes" : "No"}
                          </h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <div className="container">
              <div className="row justify-content-md-center">
                <div className="col-md-auto">
                  <div
                    className="spinner-border"
                    style={{ width: "3rem", height: "3rem" }}
                    role="status"
                  >
                    <span className="sr-only">Loading...</span>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

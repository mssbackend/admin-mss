import React, { Component } from "react";
import { browserHistory } from "react-router";
import HorizontalBarChart from "../modules/HorizontalBarChart";
import PieChart from "../modules/PieChart";
import "../styles/ActiveUsers.css";
import NetworkUtil from "../networkUtil/NetworkUtil";
import moment from "moment";
export default class ActiveUsers extends Component {
  mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      isDataloaded: false,
      bySchools: [],
      byVersion: [],
      recentUsers: []
    };
    this.getData();
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  goBack = () => {
    browserHistory.goBack();
  };
  onChange = e => {
    this.setState({ isDataloaded: false });
    this.getData(e.target.value);
  };

  async getData(action = "today") {
    var response = await new NetworkUtil().httpGet(
      "admin/GetDataForActiveUsersPage?action=" + action
    );
    if (response != null) {
      if (response.status === 200) {
        const responseJson = await response.json();
        if (responseJson["success"]) {
          if (this.mounted) {
            this.setState(responseJson["data"]);
            this.setState({ isDataloaded: true });
          }
        }
      }
    }
  }

  render() {
    return (
      <div>
        <div className="container mt-5">
          <button
            onClick={this.goBack}
            type="button"
            className="btn btn-primary"
          >
            &lt; Go back
          </button>
          <div className="form-group pt-2">
            <div className="col-sm-4 col-md-2 pl-0">
              <select onChange={this.onChange} className="form-control">
                <option value={"today"}>Today</option>
                <option value={"yesterday"}>Yesterday</option>
                <option value={"all"}>All</option>
              </select>
            </div>
          </div>
        </div>
        {this.state.isDataloaded ? (
          <div>
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <div className="card shadow">
                    <div className="card-body  p-0">
                      <HorizontalBarChart
                        title="Active users by school"
                        Xaxistitle="Schools"
                        Yaxistitle="Active users"
                        data={this.state.bySchools}
                      ></HorizontalBarChart>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card shadow">
                    <div className="card-body  p-0">
                      <PieChart
                        title="App versions"
                        data={this.state.byVersion}
                      ></PieChart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mt-5">
              <div className="row">
                <div className="col-12 col-sm-8 col-md-5">
                  <div className="card shadow">
                    <h3 className="pl-4">Recent online users</h3>
                    <ul className="list-group">
                      {this.state.recentUsers.map((value, index) => {
                        return (
                          <li
                            key={index}
                            className="border-0 list-group-item d-flex justify-content-between align-items-center"
                          >
                            <div className="image-parent">
                              {value["profile_pic"] != null ? (
                                <img
                                  height="40"
                                  width="40"
                                  src={value["profile_pic"]}
                                  alt="quixote"
                                  className="img-responsive rounded-circle"
                                />
                              ) : (
                                <div
                                  style={{
                                    height: "40px",
                                    width: "40px",
                                    backgroundColor: "#f5f5f5"
                                  }}
                                  className="rounded-circle"
                                >
                                  <p
                                    className="pt-2"
                                    style={{ textAlign: "center" }}
                                  >
                                    {value["name"].toString().substring(0, 1)}
                                  </p>
                                </div>
                              )}
                              <div
                                className="pl-3"
                                style={{ display: "block" }}
                              >
                                <h6 className="mb-0">{value["name"]}</h6>
                                <p className="text-secondary mb-0">
                                  {moment(value["connected_at"])
                                    .startOf("hour")
                                    .fromNow()}{" "}
                                  ( {value["app_version"]} )
                                </p>
                                <p className="mb-0">{value["school_name"]}</p>
                              </div>
                            </div>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="row justify-content-md-center">
              <div className="col-md-auto">
                <div
                  className="spinner-border"
                  style={{ width: "3rem", height: "3rem" }}
                  role="status"
                >
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

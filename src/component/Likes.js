import React, { Component } from "react";
import { browserHistory } from "react-router";
import HorizontalBarChart from "../modules/HorizontalBarChart";
import PieChart from "../modules/PieChart";
import NetworkUtil from "../networkUtil/NetworkUtil";

export default class Likes extends Component {
  mounted = true;
  constructor(props) {
    super(props);
    this.state = {
      isDataloaded: false,
      bySchools: [],
      byCategory: []
    };
    this.getData();
  }
  async getData(action = "today") {
    var response = await new NetworkUtil().httpGet(
      "admin/GetDataForLikesPage?action=" + action
    );
    if (response != null) {
      if (response.status === 200) {
        const responseJson = await response.json();
        if (responseJson["success"]) {
          if (this.mounted) {
            this.setState(responseJson["data"]);
            this.setState({ isDataloaded: true });
          }
        }
      }
    }
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  goBack = () => {
    browserHistory.goBack();
  };
  onChange = e => {
    this.setState({ isDataloaded: false });
    this.getData(e.target.value);
  };
  render() {
    return (
      <div>
        <div className="container mt-5">
          <button
            onClick={this.goBack}
            type="button"
            className="btn btn-primary"
          >
            &lt; Go back
          </button>
          <div className="form-group pt-2">
            <div className="col-sm-4 col-md-2 pl-0">
              <select onChange={this.onChange} className="form-control">
                <option value={"today"}>Today</option>
                <option value={"yesterday"}>Yesterday</option>
                <option value={"all"}>All</option>
              </select>
            </div>
          </div>
        </div>
        {this.state.isDataloaded ? (
          <div>
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <div className="card shadow">
                    <div className="card-body  p-0">
                      <HorizontalBarChart
                        title="Likes by schools"
                        Xaxistitle="Schools"
                        Yaxistitle="Total likes"
                        data={this.state.bySchools}
                      ></HorizontalBarChart>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="card shadow">
                    <div className="card-body  p-0">
                      <PieChart
                        title="Likes by category"
                        data={this.state.byCategory}
                      ></PieChart>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="row justify-content-md-center">
              <div className="col-md-auto">
                <div
                  className="spinner-border"
                  style={{ width: "3rem", height: "3rem" }}
                  role="status"
                >
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

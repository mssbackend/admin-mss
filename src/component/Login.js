import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "../styles/Login.css";
import { browserHistory } from "react-router";
import Globals from "../modules/globals";
import NetworkUtil from "../networkUtil/NetworkUtil";
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: "",
      password: ""
    };
    if (new Globals().checkTokenNotExpired()) {
      browserHistory.push("/main/home");
    }
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = async event => {
    this.setState({ loading: true });
    event.preventDefault();
    try {
      const response = await new NetworkUtil().httpPost(
        {
          email: this.state.email,
          password: this.state.password
        },
        "account/AdminLogin"
      );
      if (response != null) {
        if (response.status === 200) {
          const responseJson = await response.json();
          if (responseJson["success"]) {
            this.setState({ loading: false });
            localStorage.setItem(
              "accessToken",
              JSON.stringify(responseJson["data"])
            );
            browserHistory.push("/main/home");
          }
        }
      }
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    return (
      <div className="Login container">
        <div className="offset-md-4 offset-sm-4 col-md-4 col-sm-4 ">
          <div className="login-logo">
            <img
              className="img-responsive"
              height="80"
              width="80"
              alt=""
              src="logo.png"
            />
          </div>
          <div>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="email">
                <Form.Control
                  autoFocus
                  type="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </Form.Group>
              <Form.Group controlId="password">
                <Form.Control
                  value={this.state.password}
                  onChange={this.handleChange}
                  type="password"
                />
              </Form.Group>
              {!this.state.loading ? (
                <Button block disabled={!this.validateForm()} type="submit">
                  Login
                </Button>
              ) : (
                <Button
                  block
                  className="btn btn-primary"
                  type="button"
                  disabled
                >
                  <span
                    className="spinner-border spinner-border-md"
                    role="status"
                    aria-hidden="true"
                  ></span>
                </Button>
              )}
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

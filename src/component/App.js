import React, { Component } from "react";
import { Router, Route, browserHistory } from "react-router";
import Login from "../component/Login";
import Main from "../component/Main";
import Home from "../component/Home";
import ActiveUsers from "../component/AciveUsers";
import UsersActivity from "./UsersAcitivity";
import SharedFiles from "./SharedFiles";
import Likes from "./Likes";
import Comments from "./Comments";
import Schools from "./Schools";

export default class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={Login} />
        <Route path={"main"} component={Login} />
        <Route path={"main"} component={Main}>
          <Route exact path={"home"} component={Home} />
          <Route path={"schools"} component={Schools} />
          <Route path={"activeusers"} component={ActiveUsers} />
          <Route path={"usersactivity"} component={UsersActivity} />
          <Route path={"sharedfiles"} component={SharedFiles} />
          <Route path={"likes"} component={Likes} />
          <Route path={"comments"} component={Comments} />
        </Route>
      </Router>
    );
  }
}

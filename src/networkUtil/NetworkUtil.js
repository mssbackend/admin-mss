export default class NetworkUtil {
  url = "http://192.168.43.118/MSSWebApi/api/";

  getAccesToken() {
    if (localStorage.getItem("accessToken") != null) {
      var jsonData = JSON.parse(localStorage.getItem("accessToken"));
      return jsonData["token"].toString();
    }
    return "";
  }

  async httpGet(api = "", keyrequired = true) {
    this.url = this.url + api;
    var headers = {};
    headers["Content-Type"] = "application/json";
    headers["tmz"] = -new Date().getTimezoneOffset();
    if (keyrequired) {
      var token = this.getAccesToken();
      headers["Authorization"] = "bearer " + token;
    }
    try {
      const response = await fetch(this.url, {
        headers: headers
      });
      return response;
    } catch (error) {}
  }
  async httpPost(body = {}, api = "") {
    try {
      const response = await fetch(this.url + api, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          tmz: -new Date().getTimezoneOffset()
        },
        body: JSON.stringify({
          email: body.email,
          password: body.password
        })
      });
      return response;
    } catch (error) {}
  }
}
